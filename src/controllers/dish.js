// Dependencies
const Dish = require('../models/dish');
const { isEmpty } = require('../utils/index');

// Export modules
module.exports = {
  getDishes: async (req, res) => {
    try {
      const dishes = await Dish.find({ });

      if (dishes.length <= 0) {
        return res.status(404).json({ success: true, message: `OMG why we don't have dishes? thanks 4T -_-`, dishes, error: null });
      }
      return res.status(200).json({ success: true, message: `Here are your dishes fella ;D`, dishes, error: null });
    } catch (error) {
      return res.status(500).json({ success: false, message: `We got a problem getting the dishes mai fren :'(`, dishes: null, error });
    }
  },

  getDishById: async (req, res) => {
    try {
      const { dishId } = req.params;
      const dish = await Dish.findById(dishId);

      if (isEmpty(dish)) {
        return res.status(404).json({ succes: true, message: `Maybe u put the wrong id dude if not, we don't have it, sorry`, dish, error: null });
      }
      return res.status(200).json({ success: true, message: `Here is your dish mai fren ;D`, dish, error: null });
    } catch (error) {
      return res.status(500).json({ success: false, message: `We got a problem getting that dish fella :'(`, dish: null, error });
    }
  },

  newDish: async (req, res) => {
    try {
      const { name, description, sizesAndPrices, type } = req.body;

      const dish = await new Dish();
      dish.name = name;
      dish.description = description;
      dish.sizesAndPrices = sizesAndPrices;
      dish.type = type;
      await dish.save();

      res.status(201).json({ success: true, message: `I just create this dish, I know man, I'm awesome`, dish: dish, error: null });
    } catch (error) {
      res.status(500).json({ success: false, message: `Sorry man I can't create that dish, it's ur fault, u pass me a wrong recipe`, dish: null, error });
    }
  },

  deleteDish: async (req, res) => {
    try {
      const { dishId } = req.body;
      await Dish.findOneAndRemove(dishId);

      res.status(200).json({ succes: true, message: `I just got rid of a dish, don't even ask me which it is, I don't remember`, error: null});
    } catch (error) {
      res.status(200).json({ succes: false, message: `Sorry I can't delete it it's too good a dish`, error});
    }
  }
};