const express = require('express');
const dishCtrl = require('../controllers/dish');
const api = express.Router();

// Dishes
api.get('/dish', dishCtrl.getDishes);
api.get('/dish/:dishId', dishCtrl.getDishById);
api.post('/dish', dishCtrl.newDish);
api.delete('/dish/:dishId', dishCtrl.deleteDish);

module.exports = api;