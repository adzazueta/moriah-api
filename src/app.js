// Dependencies
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

// Allowed Origins
const allowedOrigins = ['https://moriah.com.mx', 'https://www.moriah.com.mx', 'https://qa.moriah.com.mx', 'http://localhost:8081'];

// Server variable
const app = express();

// API variable
const apiV1 = require('./routes');

// Middelwares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors({
  origin: (origin, callback) => {
    if (!origin) return callback(null, true);
    if (allowedOrigins.indexOf(origin) === -1) {
      const msg ='The CORS policy for this site does not allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  }
}));

// API main route
app.use('/v1', apiV1);

// Export modules
module.exports = app;
