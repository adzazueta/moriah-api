// Dependencies
const mongoose = require('mongoose');
// Sehcema variable
const Schema = mongoose.Schema;

// Dish model schema
const dishSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  sizesAndPrices: [{ type: Object, required: true }],
  type: { type: String, required: true }
}, { timestamps: true });

// Export model schema
module.exports = mongoose.model('dish', dishSchema);