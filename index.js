// Dependencies
const mongoose = require('mongoose');
const app = require('./src/app');

// DB Connection
mongoose.connect(process.env.MONGODB_URI, (error, res) => {
  if (error) {
    return console.error(`An error has ocurred: ${error}`);
  }

  console.log(`DB is Online`);

  // Initialize the server
  app.listen(process.env.PORT, () => {
    console.log(`Server on http://localhost:${process.env.PORT}`)
  })
});